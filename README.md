The Heart Rate Monitor Project has four components: 

user_file.py
----------------
The Python program integrates functionalities for all the Python programs below. It allows a user to specify certain input arguments by argparse. 

* argv[0] : Python filename
* argv[1] : Binary filename
* argv[2] : bradycardia threshold
* argv[3] : tachycardia threshold
* argv[4] : type of data used (ECG or PP)
* argv[5] : range for calculating average heart rate

test_read_binary.py
---------------------
	
* write_bin() : The Python program can be used to construct sample binary input data with customized input values. 
* test_binary(): The program can be used to test binary numpy array for invalid data type and return a flag representing the validity of data

read_binary.py
---------------------

The Python program reads binary data in uint16 windows, use the first 32 bits as sampling frequency for both ECG and plethymography, and store the rest in a numpy array.

* removeInvalid(signal): a helper program to remove NaN and Inf data from signal array
* rectify(signal): rectify signal to be all positive and valid values
* read_binary: read binary data from input file and create freq, ECG, plethymography arrays

test_hr.py
---------------------

The Python program is comprised of a testing function that run all specified tests for an input file and display elapsed signal time, instantaneous heart rate value, 1 min average heart rate, 5 mins average heart rate and indication of brady- or tachycardia conditions on the monitor screen. 

The program then runs a sine wave test program using the test() function and verifies that the both test() and est_hr.py work, based on the printed output on the monitor screens.

* test(): given specific inputs, run all tests implemented in est_hr.py and display outputs on monitor by printing out results
* test_sine(): running test for a sine wave to simulate heart rate data
* test_file(): running test for specified file to simulate heart rate data

est_hr.py
---------------------

The program is a series of function: estimated instantaneous heart rate, calculating 1 & 5 min average heart rate, recording 10 min continuous trace log of heart rate and testing for alarm signal for brady- or tachycardia conditions. 

* get_instant_hr(freq,ECG,lastPeak): 
** Calculating instantaneous heart rate data based on ECG signal, testing for alarm conditions
** The program has two pre-defined calibration values: 
***peakThreshold = 0.95, which marks that >95% of the max peak value is recognized as peaks. This helps get rid of the local minimum values, which can be recognized by the scipy find_peaks functions as peaks too.
***invalidThreshold = 200, which tests and gets rid of invalid heart rate

* avg_hr(freq,ECG,time_range): calculate constantly updated average heart rate based on 10 min trace

* continuous_hr(new_hr): continuously log/update data into 10min trace hrLog arrayList

* display(signal_time,result_instant_hr,avg1min, avg5min, condition): continuously display all required output specifications on monitor screen

* process_hr(array): calculating average of an input array of heart rates while taking out the error msgs