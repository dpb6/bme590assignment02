from test_read import *
from read import *
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import logging
import sys
import matplotlib.pyplot as plt

lastPeak = 0
hrLog = []
bradycardia_threshold = 60
tachycardia_threshold = 100
MINUTE = 60


def get_instant_hr(freq,data,lastPeak,userPeakThreshold,userInvalidThreshold):
    """calculating instantaneous heart rate data based on Data signal (either ECG or PP)

    :param freq: signal frequency for both ECG and plethymography
    :param Data: numpy array of ECG signal or PP signal
    :param lastPeak: integer value representing index of last peak found in last test
    :param userPeakThreshold: user defined peak detection threadhold value, used to overwrite default 95%
    :param userInvalidThreshold: user defined maximum valid heart rate threshold, used to overwrite default 200bpm
    :type freq: int
    :type Data: np.array
    :type lastPeak: int
    :returns: hr,condition,lastPeak: heart rate value, test for Bradycardia/Tachycardia, last peak found in current test
    :rtype: int,string, int
    """

    #Data = np.convolve(Data, Data, mode="full")

    condition = "Normal"
    
    # Convolution to smooth out data
    nw = 256
    std = 40
    window = signal.gaussian(nw, std, sym=True)
    Data = signal.convolve(data, window, mode='same') / np.sum(window)


    # x = np.linspace(0,20000,20000)
    # plt.plot(x[3200:3300],data[3200:3300])
    # plt.plot(x[3200:3300],Data[3200:3300],color="red")
    # plt.show()

    if userPeakThreshold:
        peakThreshold = userPeakThreshold   # allow user to overwrite default value
    else: 
        peakThreshold = 0.95                 # default value
    if userInvalidThreshold:
        invalidThreshold = userInvalidThreshold
    else: 
        invalidThreshold = 200

    maxData = np.amax(Data)
    peakList=[]
    # getting 1s of data
    peakind = signal.find_peaks_cwt(Data,np.arange(1,10))
    for i in range(0,len(peakind)):
        if Data[peakind[i]] >= maxData*peakThreshold:
            peakList.append(peakind[i])
    # To account for the small noise peaks
    if (len(peakList)>1):
        period = (peakList[1]-peakList[0])/freq
        lastPeak = peakList[1]
    else: 
        # Not enough peaks discovered
        try:
        	period = (peakList[0]+freq-lastPeak) / freq
        except IndexError:
        	logging.error("no valid peak found!")
        	return 0, "error", 0
    hr = 1/ period *MINUTE
    if hr < bradycardia_threshold:
        #print("DANGER: Warning for Bradycardia")
        condition = "Bradycardia"
    elif hr > tachycardia_threshold:
        #print("DANGER: Warning for Tachycardia")
        condition = "Tachycardia"
    if hr > invalidThreshold: 
        hr = "error"                       #invalid value
    print ("Instantaneous heart rate "+str(hr))
    return hr,condition,lastPeak

def avg_hr(freq,Data,time_range):
    """calculate constantly updated average heart rate based on 10 min trace

    :param freq: signal frequency for both ECG and plethymography
    :param Data: numpy array of ECG signal or PP signal
    :param time_range: range of averaging in mins (1 min or 5 mins)
    :type freq: int
    :type Data: np.array
    :type time_range: int
    :returns hr: average heart rate value
    :rtype: int
    """
    total_min = len(Data)/freq/MINUTE;
    p = time_range / total_min  #portion of data to extract

    logging.debug (len(peakind))
    #print(len(peakind)*p)
    peakind = signal.find_peaks_cwt(Data,np.arange(1,10))

    end = np.floor(len(peakind)*p)
    if (end < 1):
        logging.error ("Error: no peak detected within time range")
        return -1
    else:
        period = (peakind[int(end)]-peakind[0])/ freq / (int(end)-1)
        hr = 1/period *MINUTE
        #print("hr "+str(hr))

    logging.debug ("Average heart rate over "+str(len(Data)/freq*p)+" s "+str(hr))
    return hr

def continuous_hr(new_hr):
    """continuously log/update data into 10 min trace hrLog arrayList

    :param new_hr: the new heart rate data being logged in
    :type new_hr: float 
    :returns:null 
    """

    if (new_hr == "error"):
        return

    record_time = 10*MINUTE   # in sec: 10 mins of data
    if (len(hrLog) < record_time):
        hrLog.append(new_hr)
    else:                #if list is full, pop first element and insert live heart rate
        hrLog.pop(0)
        hrLog.insert(record_time-1,new_hr)

def display(signal_time,result_instant_hr,avg1min, avg5min, avguser, condition):
    """continuously display all required output specifications on monitor screen

    :param signal_time: elapsed signal time, updating every 10 seconds
    :param result_instant_hr: calculated instantaneous heart rate
    :param avg1min: calculated average heart rate over 1 min
    :param avg5min: calculated average heart rate over 5 mins
    :param avguser: calculated average heart rate over user customized range
    :param condition: Bradycardia/Tachycardia condition for the instantaneous heart rate data
    :type signal_time: int 
    :type result_instant_hr: float
    :type avg1min: float
    :type avg5min: float
    :type avguser: float
    :type condition:string
    :returns:null 
    """

    loggingStr = "\n"

    loggingStr += "*******************************************\n"
    loggingStr += "Elapsed Signal Time: "+str(signal_time)+" sec.\n"  #every 10s
    if (result_instant_hr == "error"):
        loggingStr += "Instantaneous Heart Rate: error\n"
    else:
        loggingStr += ("Instantaneous Heart Rate: %.3f" % result_instant_hr + "\n")
    
    loggingStr += ("1 min Average Heart Rate: %.3f" % avg1min + "\n")
    loggingStr += ("5 min Average Heart Rate: %.3f" % avg5min + "\n")
    loggingStr += ("User Specified Average Heart Rate: %.3f" % avguser + "\n")
    loggingStr += ("Bradycardia/Tachycardia Condition: "+condition+"\n")
    loggingStr += ("*******************************************\n")

    print("*******************************************\n")
    print("Elapsed Signal Time: "+str(signal_time)+" sec.\n")  #every 10s
    if (result_instant_hr == "error"):
        print("Instantaneous Heart Rate: error\n")
    else:
        print("Instantaneous Heart Rate: %.3f" % result_instant_hr + "\n")
    
    print("1 min Average Heart Rate: %.3f" % avg1min + "\n")
    print("5 min Average Heart Rate: %.3f" % avg5min + "\n")
    print("User Specified Average Heart Rate: %.3f" % avguser + "\n")
    print("Bradycardia/Tachycardia Condition: "+condition+"\n")
    print("*******************************************\n")

    logging.basicConfig(filename='unitTesting.log',level = logging.INFO, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info(loggingStr)

def process_hr(array):
    """calculating average of an input array of heart rates while taking out the error msgs

    :param array: input array whose average needs to be calculated
    :type array: numpy.array 
    :returns avg: average heart rate value
    """
    a = len(array)
    Sum = 0
    for i in range(0,a):
        if (array[i] == 'error'):
            a = a-1
        else:
            Sum += array[i]
    try: 
        avg = Sum/a
    except ZeroDivisionError:
        logging.error(" ZeroDivisionError: empty array, not enough valid instant heart rate")
        return 0
    return avg