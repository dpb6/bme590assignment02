from struct import *
import numpy as np
from numpy import abs
from test_read import *
import scipy.io as sio
import logging
import h5py
import sys


def removeInvalid(signal):
    """remove NaN and Inf data from signal

    :param signal: input signal from rectify()
    :returns: signal (np.array)
    """
    size = len(signal)
    for i in range(0,size):
        if (signal[i]=="NaN" or signal[i]=="Inf"):
            signal.pop(i)
    return signal


def rectify(signal):
    """rectify signal to be all positive

    :param signal: input signal from read_data()
    :returns: rectified_signal (np.array)
    """
    signal = removeInvalid(signal)
    rectified_signal = abs(signal)
    return rectified_signal


def extract(a):
    # Determine sample frequency:
    freq = a[0]
    # Determine two numpy arrays:
    n = int((len(a)-2)/2)
    ECG=[0]*n
    PP=[0]*n
    for i in range(2,len(a)-1):
        if i%2==0:
            ECG[int((i-2)/2)]=a[i]
        else:
            PP[int((i-3)/2)]=a[i]
    ECG = rectify(ECG)
    PP = rectify(PP)

    return freq,ECG,PP


def read_binary(filename):
    """read binary data from input file and create freq, ECG, plethymography arrays

    :param filename: input filename
    :type filename: string
    :returns: freq (int), ECG (np.array), plethy (np.array)
    """

    # Read from .bin files
    try:
        f = open(filename,"r")
    except FileNotFoundError:
        logging.error("FileNotFound")
        sys.exit()
    np_arr = np.fromfile(f,dtype='uint16') #construct array from binary data file
    (freq,ECG,PP) = extract(np_arr)

    return freq, ECG, PP


def read_HDF5(filename):
    """read HDF5 data from input file and create freq, ECG, plethymography arrays

    :param filename: input filename
    :type filename: string
    :returns: freq (int), ECG (np.array), plethy (np.array)
    """

    with h5py.File(filename, 'r') as hf:
        data = hf.get('dataset_1')
        if data is None:
            freq = np.array(hf.get('fs')).item(0)
            PP = np.array(hf.get('pp'))[0]
            ECG = np.array(hf.get('ecg'))[0]
        else:
            np_arr = np.array(data).flatten()
            (freq, ECG, PP) = extract(np_arr)

    return (freq, ECG, PP)


def read_mat(filename):
    """read Matlab v5 data from input file and create freq,
    ECG, plethymography arrays

    :param filename: input filename
    :type filename: string
    :returns: freq (int), ECG (np.array), plethy (np.array)
    """
    try:
        d = sio.loadmat(filename)
    except NotImplementedError:
        return read_HDF5(filename)

    who = sio.whosmat(filename)
    key = who[0][0]
    length = len(d[key])
    np_arr = []
    for i in range(0, length):
        np_arr.append(d[key][i][0])
    (freq, ECG, PP) = extract(np_arr)

    return (freq, ECG, PP)
