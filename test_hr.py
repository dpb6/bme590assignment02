# last, test heart rate data
from array import array
from read import *
from est_hr import *
import numpy as np

lastPeak = 0
MINUTE = 60.0

def test(freq,span,Data,lastPeak,avgRange):
	"""given specific inputs, run all tests implemented in est_hr.py and display outputs on monitor by printing out results

	:param freq: signal frequency for both ECG and plethymography
	:param span: how long the signal lasts, in second 
	:param Data: numpy array of ECG signal or PP signal
	:param lastPeak: integer value representing index of last peak found in last test
	:type freq: int
	:type span: int
	:type Data: np.array
	:type lastPeak: int
	:returns null
	"""

	avgRange = int(avgRange)
	signal_time = 0
	for i in range(0,int(60*span)):  #every sec
		secData = Data[i*freq:(i+1)*freq]
		if i % 10 == 0:   #updating signal time every 10s
			signal_time = i;
		(result_instant_hr,condition,outPeak) = get_instant_hr(freq,secData,lastPeak,0.95,200)  #updating every sec

		#print("test: instant hr "+str(result_instant_hr))

		lastPeak = outPeak 
		if (result_instant_hr == -1):
			result_instant_hr = "error"
		continuous_hr(result_instant_hr)
		#updaing last peak in case not enough peak is found
		#solved local variable referenced before assignment problem by passing in/out values
		#calculate live 1 min average
		size = len(hrLog)
		#print(size)
		if size > MINUTE: 
			avg_hr_1min = process_hr(hrLog[size-61:size-1])
		else:
			avg_hr_1min = process_hr(hrLog)
		#calculate live 5 min average
		if size > 5*MINUTE: 
			avg_hr_5min = process_hr(hrLog[size-301:size-1])
		else:
			avg_hr_5min = process_hr(hrLog)
		#calculate live n mins average based on user input 
		if size > avgRange:
			avg_hr_user = process_hr(hrLog[size-avgRange-1:size-1])
		else:
			avg_hr_user = process_hr(hrLog)
		display(signal_time,result_instant_hr,avg_hr_1min,avg_hr_5min,avg_hr_user,condition)

def test_sine():
	"""running test for a sine wave to simulate heart rate data
	"""
	freq = 1000
	f = 1.5
	avgRange = 1
	span = 2   #entire time span in mins
	t = np.arange(0,60*span,1/freq)   
	Data = np.sin(2*np.pi*f*t)
	print("Data set size "+str(len(Data)))
	test(freq, span, Data, lastPeak, avgRange)

	return "Done Sine Wave Testing."

def test_file(filename,type,avgRange):
	"""running test for specified file to simulate heart rate data

	:param filename: name of the input file
	:type filename: string
	:returns:null 
	"""

	suffix = filename[-3:]

	if suffix == "bin":
		(freq,ECG,PP) = read_binary(filename)
	elif suffix == "mat":
		(freq,ECG,PP) = read_mat(filename)
	elif suffix == ".h5":
		(freq,ECG,PP) = read_HDF5(filename)
	else: 
		logging.error ("Error: unidentified input file type")

	print(freq,ECG,PP)

	if (type == "ECG"):
		span = len(ECG)/freq/MINUTE
		test(freq,span,ECG,lastPeak,avgRange)
	elif (type == "PP"):
		span = len(PP)/freq/MINUTE
		test(freq,span,PP,lastPeak,avgRange)
	else: 
		span = len(ECG)/freq/MINUTE
		span2 = len(PP)/freq/MINUTE
		test(freq,span,ECG,lastPeak,avgRange)
		test(freq,span,PP,lastPeak,avgRange)

if __name__ == "__main__":
	#test_sine()
	print("Uncomment the following unit test cases in test_hr.py __main__ to test heart rate data")
	test_file("test1.bin","ECG",1)
	#test_file("test2.bin","ECG",1)    #errornous data, suppose to throw exception
	#test_file("test3.bin","ECG",1)
	#test_file("test4.mat","ECG",1)
	#test_file("test5.h5","ECG",1)