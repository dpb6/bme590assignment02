from array import array
import scipy.io as sio
from read import *
import numpy as np
import h5py

def write_mat():
	"""enable user to write into .mat file with customized input values

	:param sample: sample numpy array of discrete data values
	:returns: null
	"""
	m1 = np.random.random(size=50)
	sio.savemat("data01.mat",{'data':m1})

def write_HDF5(sample):
	"""enable user to write into .h5 file with customized input values

	:param sample: sample numpy array of discrete data values
	:returns: null
	"""
	m1 = sample;
	#m1 = np.random.random(size = (20,20))
 
	with h5py.File('data.h5', 'w') as hf:
		hf.create_dataset('dataset_1', data=m1)

def write_bin(sample):
	"""enable user to write into .bin file with customized input values

	:param sample: sample numpy array of discrete data values
	:returns: null
	"""
	import numpy as np

	print(sample)

	f = open('test1.bin','wb')
	x = np.int16(array('f',sample))
	f.write(x)
	f.close()

def test_binary(array):
	"""test binary numpy array for invalid data and return flag

	:param array: numpy array of either ECG or Plethymography data 
	:returns: flag string representing states of data
	:rtype: string
	"""
	size = len(array)
	flag = "ok"
	for i in range(0,size):
		if (array[i]=="NaN" or array[i]=="Inf"):
			flag = "corrupted"
	return flag

if __name__ == "__main__":

	freq = 1000
	f = 1.5
	avgRange = 1
	span = 2   #entire time span in mins
	t = np.arange(0,60*span,1/freq)   
	Data = 500 * np.sin(2*np.pi*f*t)

	print("***********TEST1.BIN***********************")
	(freq,ECG,plethy)=read_binary("test1.bin")
	print("Frequency: "+str(freq))
	print("ECG data: ")
	print(ECG)
	print ("Plethymography data: ")
	print(plethy)
	print("***********FINISH**********")
	print()

	print("***********TEST2.BIN************************")
	(freq,ECG,plethy)=read_binary("test2.bin")
	print("Frequency: "+str(freq))
	print("ECG data: ")
	print(ECG)
	print ("Plethymography data: ")
	print(plethy)
	print("***********FINISH**********")
	print()

	print("***********TEST3.BIN************************")
	(freq,ECG,plethy)=read_binary("test3.bin")
	print("Frequency: "+str(freq))
	print("ECG data: ")
	print(ECG)
	print ("Plethymography data: ")
	print(plethy)
	print("***********FINISH**********")
	print()

	print("***********TEST4.mat************************")
	(freq,ECG,plethy)=read_mat("test4.mat")
	print("Frequency: "+str(freq))
	print("ECG data: ")
	print(ECG)
	print ("Plethymography data: ")
	print(plethy)
	print("***********FINISH**********")
	print()

	print("***********TEST5.H5************************")
	(freq,ECG,plethy)=read_HDF5("test5.h5")
	print("Frequency: "+str(freq))
	print("ECG data: ")
	print(ECG)
	print ("Plethymography data: ")
	print(plethy)
	print("***********FINISH**********")
	print()