from test_hr import test_file
import argparse


def mainTesting():
    """User creates an input argument that specifies the binary data filename.
    Use this file to parse in input arguments.
    This file integrates all functions in the other files

    :param argument 0: binary filename
    :param argument 1: bradycardia threshold
    :param argument 2: tachycardia threshold
    :param argument 3: type of data used (ECG or PP)
    :param argument 4: range of data to average
    :returns: freq (int), ECG (np.array), plethy (np.array)
    """

    print("Using __main__ directly to test heart rate data for binary file")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Heart Rate Monitor.')
    parser.add_argument("--filename",
                        dest="filename",
                        default="test1.bin",
                        help="binary heart rate data filename")
    parser.add_argument("--bradycardia",
                        dest="bradycardia",
                        default=60,
                        help="bradycardia threshold")
    parser.add_argument("--tachycardia",
                        default=100, help="tachycardia threshold")
    parser.add_argument("--dataType",
                        default="ECG",
                        help="define the type of data: ECG, PP or BOTH")
    parser.add_argument("--avgRange",
                        default=1,
                        help="define the range of data you want to average")
    args = parser.parse_args()
    # print(args.filename)
    # print(args.bradycardia)
    # print(args.tachycardia)
    # print(args.dataType)
    # print(args.avgRange)
    test_file(args.filename, args.dataType, args.avgRange)
